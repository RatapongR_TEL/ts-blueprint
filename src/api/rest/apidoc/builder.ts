import { getFromContainer, MetadataStorage } from 'class-validator' // tslint:disable-line
import { validationMetadatasToSchemas } from 'class-validator-jsonschema'
import * as oa from 'openapi3-ts'
import 'reflect-metadata'
import { getMetadataArgsStorage, RoutingControllersOptions } from 'routing-controllers'
import { routingControllersToSpec } from 'routing-controllers-openapi'

export function buildSpec(info: oa.InfoObject, routingControllerOptions?: RoutingControllersOptions) {
    // Parse class-validator classes into JSON Schema:
    const metadatas = (getFromContainer(MetadataStorage) as any).validationMetadatas
    const schemas = validationMetadatasToSchemas(metadatas, {
        refPointerPrefix: '#/components/schemas/'
    })

    // Parse routing-controllers classes into OpenAPI spec:
    const storage = getMetadataArgsStorage()
    return routingControllersToSpec(storage, routingControllerOptions, {
        components: {
            schemas,
        },
        info
    })

}