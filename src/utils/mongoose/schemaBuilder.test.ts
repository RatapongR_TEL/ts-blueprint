import { Schema } from 'mongoose'
import { buildSchema } from "./schemaBuilder"

describe('schemaBuilder.test', () => {

  describe('Normal cases', () => {

    it('should not error building mongoose model from schema object', () => {
      const model = buildSchema('testmodel1', new Schema({
        title: String,
        author: String
      }))

      expect(model).toBeDefined();
      expect(model.modelName).toBe('testmodel1');
      expect(model.find).toBeDefined();
    })

    it('should not error building mongoose model from object', () => {
      const model = buildSchema('testmodel2', {
        title: String,
        author: String
      })

      expect(model).toBeDefined();
      expect(model.modelName).toBe('testmodel2');
      expect(model.find).toBeDefined();
    })

  })
})