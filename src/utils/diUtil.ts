import Container, { ObjectType } from 'typedi'

export function objectToDiValues(obj: any, prefix: string) {
  const values: { id: string, value: any }[] = []

  Object.entries(obj).forEach(([k, v]) => {
    if (typeof v === 'object') {
      values.push(...objectToDiValues(v, `${prefix}${k}.`))
    }

    values.push({
      id: `${prefix}${k}`,
      value: v
    })
  })

  return values
}

export function getOptionalDependency<T>(type: ObjectType<T>): T | undefined;
export function getOptionalDependency<T>(id: string): T | undefined;

export function getOptionalDependency<T>(typeOfId: any): T | undefined {
  if (Container.has(typeOfId)) {
    return Container.get(typeOfId);
  }

  return undefined;
}

export function getDependency<T>(type: ObjectType<T>): T;
export function getDependency<T>(id: string): T;
export function getDependency<T>(typeOfId: any): T {
  return Container.get(typeOfId);
}