import Container from 'typedi';
import { getDependency, getOptionalDependency, objectToDiValues } from "./diUtil";

jest.mock('typedi')

describe('diUtil.test', () => {

  describe('objectToDiValues', () => {

    it('should convert object key values into di format', () => {
      const results = objectToDiValues({ key1: 'value1', key2: 'value2' }, 'prefix1.');

      expect(results).toHaveLength(2);
      expect(results[0]).toEqual({ id: 'prefix1.key1', value: 'value1' });
      expect(results[1]).toEqual({ id: 'prefix1.key2', value: 'value2' });
    })

  })

  describe('getOptionalDependency', () => {
    const mockContainerGet = Container.get as jest.Mock;
    const mockContainerHas = Container.has as jest.Mock;

    beforeEach(() => {
      mockContainerGet.mockReset();
      mockContainerHas.mockReset();
    })

    it('should return undefined if object does not exist', () => {
      mockContainerHas.mockReturnValue(false);
      mockContainerGet.mockReturnValue('value');

      const result = getOptionalDependency('testdept')

      expect(result).toBeUndefined();
    })

    it('should not be undefined if object exists', () => {
      mockContainerHas.mockReturnValue(true);
      mockContainerGet.mockReturnValue('value');

      const result = getOptionalDependency('testdept')

      expect(result).toBe('value');
    })

  })

  describe('getDependency', () => {
    const mockContainerGet = Container.get as jest.Mock;

    beforeEach(() => {
      mockContainerGet.mockReset();
    })

    it('should get by id name', () => {
      mockContainerGet.mockReturnValue('value');

      const result = getDependency('testdept')

      expect(mockContainerGet).toBeCalledWith('testdept');
      expect(result).toBe('value');
    })

    it('should get by id name', () => {
      class MockClass { }

      const mockObj = new MockClass();
      mockContainerGet.mockReturnValue(mockObj);

      const result = getDependency(MockClass)

      expect(mockContainerGet).toBeCalledWith(MockClass);
      expect(result).toBe(mockObj);
    })

  })

})