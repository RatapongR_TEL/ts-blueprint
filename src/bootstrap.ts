// !!!Important note!!!
// Import sequence in this file matters
import "reflect-metadata"; // enable dependency injection using type di
import './bootstrapCache';
// boot essentials first
import './bootstrapConfig';
// boot repository
// import './bootstrapMongo';
import './bootstrapInMemory'; // comment this line and uncomment above line to use real mongo as a database
// import './bootstrapAmqp';
// import './bootstrapKafka';
import './bootstrapLogger';
// boot application
// import './bootstrapAuthentication'
// import './bootstrapAuthorization'
import './bootstrapRestApi';
import './bootstrapTracer';





