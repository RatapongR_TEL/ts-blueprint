import { Service } from "typedi";
import { kafkaConsumer } from '../../libraries/kafka/decorators/kafkaConsumer';
import { kafkaProducer } from '../../libraries/kafka/decorators/kafkaProducer';
import { ConsumeMessage, ProduceMessage } from '../../libraries/kafka/kafkaBroker';



export interface IOrderSample {
  _id: string,
  addressId: string,
  createdAt: Date,
  updatedAt: Date,
}


@Service()
export class KafkaDemoService {


  @kafkaProducer({
    topic: `tel-topic`,
    config: {
      allowAutoTopicCreation: true,
    },
  })
  async sendProducerSimple(order: { id: string }): Promise<ProduceMessage> {
    return {
      message: order,
      config: (order as any)?.config
    }
  }

  @kafkaProducer(`tel-topic-error`)
  async sendProducerSimpleError(order: { id: string }): Promise<ProduceMessage> {
    return {
      message: order
    }
  }

  @kafkaProducer(`tel-topic-error-dead-letter`)
  async sendProducerSimpleErrorDeadLetter(order: { id: string }): Promise<ProduceMessage> {
    return {
      message: order
    }
  }

  @kafkaConsumer({
    groupId: `tel-topic-group`,
    topic: `tel-topic`,
    subscribe: {
      fromBeginning: true,
    }
  })
  async receiveConsumerSimple(msg: ConsumeMessage) {
    console.log("🦻🦻🦻 receiveConsumerSimple key", msg)
    return msg
  }

  @kafkaConsumer({
    groupId: `tel-topic-error-group`,
    topic: `tel-topic-error`,
    subscribe: {
      fromBeginning: true,
    },
    options: {
      allowAutoTopicCreation: true,
    },
    errorStrategy: {
      type: "RETRY",
      retry: {
        retries: 3,
        initialRetryTime: 1000 // set delay retry
      }
    }
  })
  async receiveConsumerSimpleError(msg: ConsumeMessage) {
    console.log("🦻🦻🦻 receiveConsumerSimpleError working", msg)
    throw new Error(`🔥🔥 Failure to processing 😭`)
  }

  @kafkaConsumer({
    groupId: `tel-topic-dead-letter-group`,
    topic: `tel-topic-error-dead-letter`,
    subscribe: {
      fromBeginning: true,
    },
    errorStrategy: {
      type: "RETRY_THEN_DEAD_LETTER",
      deadLetterTopic: "tel-topic-dead-letter",
      retry: {
        retries: 3,
        // initialRetryTime: 1000 // set delay retry
      }

    }
  })
  async receiveConsumerErrorThenProduceDeadLetter(msg: ConsumeMessage) {
    console.log("🦻🦻🦻 receiveConsumerSimpleError working", msg)
    throw new Error(`🔥🔥 Failure to processing 😭`)
  }
}