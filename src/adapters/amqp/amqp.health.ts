import { AmqpConnection } from '../../libraries/amqp/connection';
import { IHealthCheck } from '../../libraries/health/interface';

export class AmqpHealthCheck implements IHealthCheck {
  constructor(private amqpCon: AmqpConnection) {};

  getName() {
    return 'amqp';
  }

  async getHealth() {
    const { status, statusMessage } = this.amqpCon.status;
    const ok = status === 'connected';

    return {
      ok,
      status,
      statusText: statusMessage?.message,
    }
  }
}
