import { MongoHealthCheck } from './adapters/mongo/mongo.health';
import { config } from './bootstrapConfig';
import { defaultLogger } from './bootstrapLogger';
import { Healthchecker } from './libraries/health/healthchecker';
import { MongoConnection } from './libraries/mongo/mongoConnection';
import { instrumentMongoose } from './libraries/tracing/instruments/mongoose';
import { ensureConfigKeys } from './utils/configUtil';
import { getDependency } from './utils/diUtil';

try {
  ensureConfigKeys(config.database, 'MONGO_URI')

  const mongooseOptions = {
    dbName: config.database.MONGO_DATABASE_NAME,
    user: config.database.MONGO_USERNAME,
    pass: config.database.MONGO_PASSWORD,
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  };

  const mongoose = new MongoConnection({
    uri: config.database.MONGO_URI!,
    options: mongooseOptions,
  }, defaultLogger);

  // instrument mongoose
  instrumentMongoose();

  // load all repositories
  import('./adapters/mongo/repositories'); // register all mongo repositories

  mongoose.connect();

  // register mongoose health check
  const healthcheck = getDependency(Healthchecker)
  healthcheck.addChecker(new MongoHealthCheck());

} catch (error) {
  defaultLogger.error(error, { event: 'bootstrap_mongo' })

  process.exit(-1)
}