import { AxiosPromise, AxiosRequestConfig, AxiosResponse } from 'axios';
import { IConfig, RequestError, ResponseError, RestClient } from './restClient';

function getResponseData(data: any, status: number = 200, statusText: string = 'ok') {
  return { data, status, statusText: statusText, headers: {}, config: {} }
}

describe('RestClient', () => {
  const mockAdapter = jest.fn<AxiosPromise<any>, [AxiosRequestConfig]>();

  const buildClient = (config?: IConfig) => {
    return new RestClient(Object.assign({}, { adapter: mockAdapter }, config));
  }

  const mockErrorResponse = (data: any, status: number = 500, statusText?: string) => {
    mockAdapter.mockRejectedValue({ response: getResponseData(data, status, statusText) });
  }

  const mockErrorRequest = (message: string) => {
    const err = new Error(message) as any;
    err.request = {};

    mockAdapter.mockRejectedValue(err);
  }

  const mockResponse = (data: any, status: number = 200) => {
    mockAdapter.mockResolvedValue(getResponseData(data, status));
  }

  beforeEach(() => {
    mockAdapter.mockReset();
    mockAdapter.mockResolvedValue(getResponseData({})); // default value
  })

  describe('Normal cases', () => {

    test.each([
      ['get'],
      ['post'],
      ['put'],
      ['delete'],
    ])('should request with correct "%s" method', async (method: any) => {
      const client = buildClient();

      // @ts-ignore
      await client[method]('https://test');

      expect(mockAdapter).toBeCalled();
      expect(mockAdapter.mock.calls[0][0]).toMatchObject<AxiosRequestConfig>(
        {
          method,
          url: 'https://test',
        });
    });

    it('should get with correct configuration with default configurations', async () => {
      const client = buildClient({
        baseURL: 'https://testBaseUrl.com',
        timeout: 3000,
        headers: {
          'x-head': 'ahead'
        },
      });
      await client.get('/test');

      expect(mockAdapter).toBeCalled();
      expect(mockAdapter.mock.calls[0][0]).toMatchObject<AxiosRequestConfig>(
        {
          method: 'get',
          baseURL: 'https://testBaseUrl.com',
          url: '/test',
          timeout: 3000,
          headers: {
            'x-head': 'ahead'
          }
        });
    })


    it('should return correct value', async () => {
      mockResponse('test');

      const client = buildClient();
      const result = await client.get('');

      expect(result).toBe('test');
    })

    it('should return null when not found', async () => {
      mockErrorResponse(undefined, 404);

      const client = buildClient({
        notFoundAsNull: true
      });
      const result = await client.get('');

      expect(result).toBe(null);
    })

  })

  describe('Error cases', () => {

    it('should return response error when not found', async () => {
      mockErrorResponse(undefined, 404, 'not found');

      const client = buildClient();
      const result = client.get('');

      await expect(result).rejects.toThrowError(ResponseError);
      await expect(result).rejects.toMatchObject<Partial<AxiosResponse>>({ status: 404 })
      await expect(result).rejects.toThrowError('404 not found');
    })

    it('should return response error when server error', async () => {
      mockErrorResponse('test', 500, 'internal server error');

      const client = buildClient();
      const result = client.get('');

      await expect(result).rejects.toThrowError(ResponseError);
      await expect(result).rejects.toMatchObject<Partial<AxiosResponse>>({ status: 500, data: 'test', statusText: 'internal server error' })
      await expect(result).rejects.toThrowError('500 internal server error');
    })

    it('should return request error', async () => {
      mockErrorRequest('request config error');

      const client = buildClient();
      const result = client.get('');

      await expect(result).rejects.toThrowError(RequestError);
      await expect(result).rejects.toThrowError('request config error');
    })

    it('should return other error', async () => {
      mockAdapter.mockRejectedValue(new Error('an error occurred'));

      const client = buildClient();
      const result = client.get('');

      await expect(result).rejects.toThrowError('an error occurred');
    })

  })

})