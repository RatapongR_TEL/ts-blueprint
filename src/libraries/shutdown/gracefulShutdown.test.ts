import { anyFunction, mock, mockReset } from 'jest-mock-extended';
import * as gc from 'node-graceful-shutdown';
import { ILogger } from '../logger/logger.interface';
import { gracefulShutdown } from "./gracefulShutdown";

jest.mock('node-graceful-shutdown');

describe('gracefulShutdown.test', () => {
  const mockShutdown = mock(gc).onShutdown;
  const mockLogger = mock<ILogger>();

  beforeEach(() => {
    mockReset(mockLogger);
    mockReset(mockShutdown);
  })

  describe('Normal cases', () => {

    it('should shutdown asynchronouselly normally', async () => {
      // prepare
      let triggerShutdown: Function | undefined;

      mockShutdown.mockImplementationOnce(async (_name, _deps, handler) => {
        triggerShutdown = handler;
      });

      const mockTestShutdown = jest.fn();

      // run test
      gracefulShutdown(mockLogger, 'test1', async () => {
        return mockTestShutdown();
      })

      // assert
      expect(mockShutdown).toBeCalledWith('test1', undefined, anyFunction());
      expect(triggerShutdown).toBeDefined();

      // trigger shutdown
      await triggerShutdown!();

      // assert
      expect(mockLogger.info).toBeCalledTimes(2);
      expect(mockLogger.error).not.toBeCalled();
      expect(mockTestShutdown).toBeCalled();

    })

    it('should shutdown with callback normally', async () => {
      // prepare
      let triggerShutdown: Function | undefined;

      mockShutdown.mockImplementationOnce(async (_name, _deps, handler) => {
        triggerShutdown = handler;
      });

      // run test
      gracefulShutdown(mockLogger, 'test1', (doneCb) => {
        doneCb();
      })

      // assert
      expect(mockShutdown).toBeCalledWith('test1', undefined, anyFunction());
      expect(triggerShutdown).toBeDefined();

      // trigger shutdown
      await triggerShutdown!();

      // assert
      expect(mockLogger.info).toBeCalledTimes(2);
      expect(mockLogger.error).not.toBeCalled();

    })

    it('should shutdown with dependency', async () => {
      // run test
      gracefulShutdown(mockLogger, 'test1', ['dept1', 'dept2'], async () => {
        return;
      })

      // assert
      expect(mockShutdown).toBeCalledWith('test1', ['dept1', 'dept2'], anyFunction());
    })

  })

  describe('Error cases', () => {

    it('should throw error when shutdown function throw error async', async () => {
      // prepare
      let triggerShutdown: Function | undefined;

      mockShutdown.mockImplementationOnce(async (_name, _deps, handler) => {
        triggerShutdown = handler;
      });

      // run test
      gracefulShutdown(mockLogger, 'test1', async () => {
        throw new Error('test error');
      })

      // assert
      expect(mockShutdown).toBeCalledWith('test1', undefined, anyFunction());
      expect(triggerShutdown).toBeDefined();

      // trigger shutdown
      await expect(triggerShutdown!()).rejects.toThrow('test error');

      // assert
      expect(mockLogger.info).toBeCalledTimes(1);
      expect(mockLogger.error).toBeCalledTimes(1);

    })

    it('should throw error when shutdown function throw error callback', async () => {
      // prepare
      let triggerShutdown: Function | undefined;

      mockShutdown.mockImplementationOnce(async (_name, _deps, handler) => {
        triggerShutdown = handler;
      });

      // run test
      gracefulShutdown(mockLogger, 'test1', (doneCb) => {
        doneCb(new Error('test error'));
      })

      // assert
      expect(mockShutdown).toBeCalledWith('test1', undefined, anyFunction());
      expect(triggerShutdown).toBeDefined();

      // trigger shutdown
      await expect(triggerShutdown!()).rejects.toThrow('test error');

      // assert
      expect(mockLogger.info).toBeCalledTimes(1);
      expect(mockLogger.error).toBeCalledTimes(1);
    })

  })

  describe('Exceptional cases', () => {

    it('should throw error when shutdown function throw error unexpectedly', async () => {
      // prepare
      let triggerShutdown: Function | undefined;

      mockShutdown.mockImplementationOnce(async (_name, _deps, handler) => {
        triggerShutdown = handler;
      });

      // run test
      gracefulShutdown(mockLogger, 'test1', () => {
        throw new Error('unexpect error')
      })

      // assert
      expect(triggerShutdown).toBeDefined();

      // trigger shutdown
      await expect(triggerShutdown!()).rejects.toThrow('unexpect error');

      // assert
      expect(mockLogger.info).toBeCalledTimes(1);
      expect(mockLogger.error).toBeCalledTimes(1);
    })

  })

})