import { plugin, Query, Schema } from 'mongoose';
import * as shimmer from 'shimmer';
import { traceUtil } from '../../../bootstrapTracer';

function startSpan(query: any) {
  const currentSpan = traceUtil.getCurrentSpan();
  const { op, mongooseCollection } = query;

  if (currentSpan && op && mongooseCollection) {
    const tags = { op, collection: mongooseCollection.name };
    const logs = [{ filter: query.getFilter(), options: query.getOptions() }];

    return traceUtil.startSpan(`mongoose - ${op}`, { currentSpan, tags, logs });
  }

  return undefined;
}

function wrapQuery(fnName: string) {
  // tslint:disable-next-line: no-function-expression
  shimmer.wrap((Query as any).prototype, fnName, function (original: any) {
    return function (this: any) {
      if (!this._span) {
        this._span = startSpan(this);
      }

      return original.apply(this, arguments)
    }
  });
}

function setPlugin(methods: string[]) {
  plugin((schema: Schema) => {
    // normal case
    // tslint:disable-next-line: no-function-expression
    schema.post(methods as any, function (this: any, _res: any) {
      if (this._span) {
        traceUtil.finishSpan(this._span);
      }
    })

    // error handling
    // tslint:disable-next-line: no-function-expression
    schema.post(methods as any, function (this: any, error: any, _res: any, next: Function) {
      if (this._span) {
        traceUtil.finishSpan(this._span, error);
      }
      next();
    })
  })
}

export const instrumentMongoose = () => {
  const wrapMethods = [
    'find',
    'findOne',
    'findOneAndRemove',
    'findOneAndUpdate',
  ];

  wrapMethods.forEach(fnName => wrapQuery(fnName))
  setPlugin(wrapMethods);
}
