import { IHealth, IHealthCheck } from './interface';

export class Healthchecker {
  private checkers: IHealthCheck[] = [];

  addChecker(...checker: IHealthCheck[]) {
    this.checkers.push(...checker);
  }

  async check(): Promise<IHealth> {
    const overallHealth: IHealth = {
      ok: true,
      details: {}
    };

    for (const checker of this.checkers) {
      const result = await this.getHealth(checker);
      overallHealth.details![checker.getName()] = result;

      if (!result.ok) {
        overallHealth.ok = false
      }
    }

    return overallHealth;
  }

  private getHealth(checker: IHealthCheck): Promise<IHealth> {
    return checker.getHealth()
      .catch(err => {
        return { ok: false, status: 'error', statusText: err.toString() }
      })
  }

}