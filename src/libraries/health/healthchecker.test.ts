import { mock } from 'jest-mock-extended';
import { Healthchecker } from "./healthchecker";
import { IHealthCheck } from './interface';

describe('healthchecker.test', () => {

  describe('Normal cases', () => {

    it('should return overall health with default value', async () => {
      const checker = new Healthchecker();

      const result = await checker.check();

      expect(result).toBeDefined();
      expect(result.ok).toBe(true);
    })

    it('should return detail health', async () => {
      const mockChecker = mock<IHealthCheck>();
      mockChecker.getName.mockReturnValue('mock_health');
      mockChecker.getHealth.mockResolvedValue({
        ok: true,
        status: 'status1',
        statusText: 'status1 text'
      });

      const checker = new Healthchecker();
      checker.addChecker(mockChecker);

      const result = await checker.check();

      expect(result).toBeDefined();
      expect(result.ok).toBe(true);
      expect(result.details).toBeDefined();
      expect(result.details!['mock_health']).toMatchObject({
        ok: true,
        status: 'status1',
        statusText: 'status1 text'
      })
    })

    it('should return unhealthy when one health check unhealthy', async () => {
      const mockHealthyChecker = mock<IHealthCheck>();
      mockHealthyChecker.getName.mockReturnValue('mock_healthy');
      mockHealthyChecker.getHealth.mockResolvedValue({
        ok: true,
        status: 'status1',
        statusText: 'status1 text'
      });

      const mockUnhealthyChecker = mock<IHealthCheck>();
      mockUnhealthyChecker.getName.mockReturnValue('mock_unhealthy');
      mockUnhealthyChecker.getHealth.mockResolvedValue({
        ok: false,
      });

      const checker = new Healthchecker();
      checker.addChecker(mockHealthyChecker, mockUnhealthyChecker);

      const result = await checker.check();

      expect(result).toBeDefined();
      expect(result.ok).toBe(false);
      expect(result.details).toBeDefined();
      expect(result.details!['mock_healthy']).toBeDefined();
      expect(result.details!['mock_unhealthy']).toBeDefined();
    })

  })

  describe('Error cases', () => {

    it('should return unhealthy result when a checker return error', async () => {
      const mockChecker = mock<IHealthCheck>();
      mockChecker.getName.mockReturnValue('mock_error');
      mockChecker.getHealth.mockRejectedValue(new Error('check error'));

      const checker = new Healthchecker();
      checker.addChecker(mockChecker);

      const result = await checker.check();

      expect(result).toBeDefined();
      expect(result.ok).toBe(false);
      expect(result.details).toBeDefined();
      expect(result.details!['mock_error']).toMatchObject({
        ok: false,
        status: 'error',
        statusText: 'Error: check error'
      })
    })

  })

})