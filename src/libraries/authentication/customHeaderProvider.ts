import { IAnonoymousableUser, IUserRepository } from './type';

export interface ICustomHeaderOptions {
  userIdKey: string
  anonymouseKey?: string
}

export class CustomHeaderAuthenticationProvider<T extends IAnonoymousableUser> {
  constructor(
    private userProvider: IUserRepository<T>,
    private options: ICustomHeaderOptions = {
      userIdKey: 'x-consumer-username',
      anonymouseKey: 'x-anonymous-consumer'
    }) { }

  async getUserFromCustomHeader(headers: any): Promise<T | undefined> {
    const { userIdKey, anonymouseKey } = this.options;

    const userId = headers[userIdKey];

    if (!userId) {
      return;
    }

    const user = await this.userProvider.getUserById(userId);

    if (user && anonymouseKey) {
      user.anonymous = !!headers[anonymouseKey];
    }

    return user;
  }
}