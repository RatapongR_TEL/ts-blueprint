import { ICustomHeaderOptions } from './customHeaderProvider';
import { IJwtOptions } from './jwtProvider';

export interface IAuthenticationOptions {
  allowAnonymous?: boolean
  useCustomHeader?: boolean
  customHeaderOptions?: ICustomHeaderOptions
  useJwt?: boolean
  jwtOptions?: IJwtOptions
}

export interface IAuthenticationContext {
  headers: any
}

export interface IAnonoymousableUser {
  anonymous?: boolean
}

export interface IUserRepository<T> {
  getUserById(id: string): Promise<T | undefined>
}