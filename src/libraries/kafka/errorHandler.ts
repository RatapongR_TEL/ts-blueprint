import { ILogger } from '../logger/logger.interface';
import { KafkaErrorRetry } from './errorTypes';
import { KafkaBroker } from './kafkaBroker';
import { PayloadHandler } from './payloadHandler';
import { ErrorHandlingStrategy, RETRY, RETRY_THEN_DEAD_LETTER } from './type';


export async function handleOnMessageError(
  error: any,
  payloadHandler: PayloadHandler,
  logger: ILogger,
  broker: KafkaBroker,
  strategy: ErrorHandlingStrategy = {
    type: RETRY,
    retry: { retries: 3 }
  }
) {

  switch (strategy.type) {
    case RETRY:
      return retryStrategy(error, payloadHandler, strategy.retry.retries, logger)

    case RETRY_THEN_DEAD_LETTER:
      return retryThenDeadLetter(
        error,
        logger,
        payloadHandler,
        broker,
        strategy.deadLetterTopic,
        strategy.retry.retries,
      )

    default:
      return retryStrategy(error, payloadHandler, strategy, logger)

  }
}

async function retryStrategy(
  error: any,
  payloadHandler: PayloadHandler,
  retryCount: number = 1,
  logger: ILogger
) {

  const maxRetry = retryCount
  if (payloadHandler.retryCount() < maxRetry) {
    payloadHandler.increaseRetry()
    logger.error(error, { event: "kafka_retry_consume", payload: payloadHandler.toString() })
    throw new KafkaErrorRetry(error.message)
  }

  await payloadHandler.commit();
  payloadHandler.clearRetry()

  logger.error(error, { event: "kafka_retries_exhausted", payload: payloadHandler.toString() })
}

async function retryThenDeadLetter(
  error: any,
  logger: ILogger,
  payloadHandler: PayloadHandler,
  broker: KafkaBroker,
  topicDeadLetter: string = "default-dead-letter-topic",
  retryCount: number = 1,
) {


  const maxRetry = retryCount
  if (payloadHandler.retryCount() < maxRetry) {
    payloadHandler.increaseRetry()
    logger.error(error, { event: "kafka_retry_consume", payload: payloadHandler.toString() })
    throw new KafkaErrorRetry(error.message)
  }

  const { headers, key, value } = broker.deSerialize(payloadHandler.getPayload().message);

  await broker.produceTransaction(
    { topic: topicDeadLetter },
    payloadHandler.getId(),
    {
      headers,
      key,
      value
    },
    async () => {
      await payloadHandler.commit();
      payloadHandler.clearRetry();
      logger.info(error, { event: "kafka_retries_exhausted", payload: payloadHandler.toString() })
    },
  )


}