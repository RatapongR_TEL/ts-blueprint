import { IKafkaConsumerConfig } from './decorators/kafkaConsumer';
import { IKafkaProducer } from './decorators/kafkaProducer';

export interface IProducerMetas extends IKafkaProducer {
}

export interface IConsumerMetas {
  consumerConfig: IKafkaConsumerConfig,
  targetConstructor: object,
  propertyKey: string
}

export class MetaDataRegistry {
  static consumerMetas: IConsumerMetas[] = [];
  static producerMetas: IProducerMetas[] = [];

  static REGISTER_PRODUCER(produceConfig: IKafkaProducer) {
    MetaDataRegistry.producerMetas.push(produceConfig)
  }

  static REGISTER_CONSUMER(
    consumerConfig: IKafkaConsumerConfig,
    targetConstructor: Object,
    propertyKey: string
  ) {

    MetaDataRegistry.consumerMetas.push({
      consumerConfig,
      targetConstructor,
      propertyKey
    })
  }
}