import { mock } from 'jest-mock-extended';
import { Consumer } from 'kafkajs';
import { ILogger } from '../logger/logger.interface';
import { handleOnMessageError } from './errorHandler';
import { KafkaErrorRetry } from './errorTypes';
import { KafkaBroker } from './kafkaBroker';
import { PayloadHandler } from './payloadHandler';
import { RETRY } from './type';


describe('errorHandler function', () => {

  it(`RETRY strategy throw KafkaErrorRetry when 'retryCount' less than 'maxRetry'`, async () => {

    const kafkaBroker = mock<KafkaBroker>();
    const consumerMock = mock<Consumer>();
    const logger = mock<ILogger>();

    const throwError = new Error("test-error")
    const payloadHandler = new PayloadHandler(
      {
        groupId: "test-group-id",
        subscribe: {
          topic: "test-consume"
        },
        errorStrategy: {
          type: RETRY, retry: { retries: 3 }
        }
      },
      consumerMock,
      {
        topic: "test-consume",
        partition: 0,
        message: {
          key: Buffer.from("test-key"),
          headers: { "test-id": "1" },
          value: Buffer.from(JSON.stringify({ test: "test" })),
          timestamp: "1",
          size: 10,
          attributes: 10,
          offset: "0",
        }
      }
    )

    expect(handleOnMessageError(
      throwError,
      payloadHandler,
      logger,
      kafkaBroker,
    )).rejects.toThrowError(KafkaErrorRetry)

  });


  it(`RETRY strategy clear retry when 'retryCount' greater than 'maxRetry'`, async () => {

    const kafkaBroker = mock<KafkaBroker>();
    const consumerMock = mock<Consumer>();
    const logger = mock<ILogger>();

    const throwError = new Error("test-error")
    const payloadHandler = new PayloadHandler(
      {
        groupId: "test-group-id",
        subscribe: {
          topic: "test-consume"
        },
        errorStrategy: {
          type: RETRY, retry: { retries: 0 }
        }
      },
      consumerMock,
      {
        topic: "test-consume",
        partition: 0,
        message: {
          key: Buffer.from("test-key"),
          headers: { "test-id": "1" },
          value: Buffer.from(JSON.stringify({ test: "test" })),
          timestamp: "1",
          size: 10,
          attributes: 10,
          offset: "0",
        }
      }
    )



    await handleOnMessageError(
      throwError,
      payloadHandler,
      logger,
      kafkaBroker,
      {
        type: RETRY, retry: { retries: 0 }
      }
    )

    expect(consumerMock.commitOffsets).toHaveBeenCalled()




  });
});