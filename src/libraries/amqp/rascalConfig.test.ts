import { IAmqpConnectionOptions } from '.';
import { buildConfig } from './rascalConfig';
import { IConsumerMeta, IPublisherMeta } from './type';


function buildAmqpConnection(config = {}): IAmqpConnectionOptions {
  return Object.assign({
    connectionUrl: "amqp://localhost:5672//test?heartbeat=5,amqp://localhost:5673//test?heartbeat=5,amqp://localhost:5674//test?heartbeat=5",
    username: 'test',
    password: 'test',
    prefix: 'test.'
  }, config)

}

describe("RascalConfig Exchanges", () => {

  it(`should build publications only when not used subscriptions`, () => {

    const mockPubliserMeta = <IPublisherMeta>{
      pubConfig: {
        exchange: "test-exchange",
        exchangeConfig: { type: "direct" },
      }
    }

    const result = buildConfig({
      ...buildAmqpConnection(),
      publisherMetas: [mockPubliserMeta]
    })
    expect(result?.vhosts?.["/"].publications).toEqual(
      expect.objectContaining({ 'test-exchange': { exchange: 'test.test-exchange' } })
    )

    expect(result?.vhosts?.["/"].subscriptions).toEqual({})
    expect(result?.vhosts?.["/"].bindings).toEqual({})

  })

  it(`should build subscriptions only when not used publications`, () => {

    const mockSubscriptionMeta = <IConsumerMeta>{
      queueConfig:
      {
        queueName: 'test-queue',
        exchange: 'test-exchange',
        routingKey: "routing.*",
        exchangeConfig: { type: "topic" }
      }
    }

    const result = buildConfig({
      ...buildAmqpConnection(),
      consumerMetas: [mockSubscriptionMeta]
    })


    expect(result?.vhosts?.["/"].publications).toEqual({})
    expect(result?.vhosts?.["/"].subscriptions).toEqual(expect.objectContaining({
      "test-queue": {
        "queue": "test.test-queue"
      }
    }))

    expect(result?.vhosts?.["/"].bindings).toEqual(expect.objectContaining({
      "test-exchange-test-queue": {
        "source": "test.test-exchange",
        "destination": "test.test-queue",
        "bindingKey": "routing.*"
      }
    }))

  })



  it(`should build 'direct' exchange type`, () => {
    const mockConsumerMeta = <IConsumerMeta>{ queueConfig: { queueName: 'test-queue', exchange: 'test-exchange', exchangeConfig: { type: "direct" } } }
    const mockPubliserMeta = <IPublisherMeta>{
      pubConfig: {
        exchange: "test-exchange",
        exchangeConfig: { type: "direct" },
      }
    }

    const result = buildConfig({
      ...buildAmqpConnection(),
      consumerMetas: [mockConsumerMeta],
      publisherMetas: [mockPubliserMeta]
    })
    const config = result?.vhosts?.["/"]

    const expectedRascalConfig = ["exchanges", "connections", "queues", "bindings", "subscriptions", "publications"]
    expect(Object.keys(config as string[]).sort()).toEqual(expectedRascalConfig.sort())
    expect(config?.exchanges).toEqual(expect.objectContaining({
      'test.test-exchange':
        { type: 'direct' }
    }))
  })


  it(`should build 'topic' exchange type`, () => {
    const mockConsumerMeta = <IConsumerMeta>{
      queueConfig:
      {
        queueName: 'test-queue',
        exchange: 'test-exchange',
        routingKey: "routing.*",
        exchangeConfig: { type: "topic" }
      }
    }
    const mockPubliserMeta = <IPublisherMeta>{
      pubConfig: {
        queue: "test-queue",
        exchange: "test-exchange",
        routingKey: "routing-test",
        exchangeConfig: {
          type: "topic",
        },
      }
    }

    const result = buildConfig({
      ...buildAmqpConnection(),
      consumerMetas: [mockConsumerMeta],
      publisherMetas: [mockPubliserMeta]
    })
    const config = result?.vhosts?.["/"]

    expect(config?.exchanges).toEqual(expect.objectContaining({
      'test.test-exchange':
        { type: 'topic' }
    }))

    expect(config?.bindings).toEqual(expect.objectContaining({
      "test-exchange-test-queue": {
        "source": "test.test-exchange",
        "destination": "test.test-queue",
        "bindingKey": "routing.*"
      }
    }))

    expect(config?.publications).toEqual(expect.objectContaining({
      "test-exchange": {
        "queue": "test.test-queue",
        "exchange": "test.test-exchange",
        "routingKey": "routing-test"
      }
    }))
  })
})


describe("RascalConfig DeadLetter", () => {

  it(`should build dead - letter config when consumer set options`, () => {

    const mockConsumerMeta = <IConsumerMeta>{
      queueConfig:
      {
        queueName: 'test-queue',
        exchange: 'test-exchange',
        exchangeConfig: { type: "direct" },
        options: {
          deadLetterExchange: 'test-dead-letter-exchange',
          messageTtl: 10000,

        }
      }
    }

    const mockConsumerDeadLetterExchange = <IConsumerMeta>{
      queueConfig: {
        queueName: "test-queue-dead-letter",
        exchange: "test-dead-letter-exchange"
      }
    }

    const mockConsumerDeadLetterExchangeWithArguments = <IConsumerMeta>{
      queueConfig: {
        queueName: 'test-queue-arguments',
        exchange: 'test-exchange',
        exchangeConfig: { type: "direct" },
        options: {
          arguments: {
            "x-message-ttl": 10000,
            "x-dead-letter-exchange": "test-dead-letter-exchange"
          }
        }
      }

    }

    const mockPubliserMeta = <IPublisherMeta>{
      pubConfig: {
        queue: "test-queue",
        exchange: "test-exchange",
        exchangeConfig: {
          type: "topic",
        },
      }
    }

    const result = buildConfig({
      ...buildAmqpConnection(),
      consumerMetas: [mockConsumerMeta, mockConsumerDeadLetterExchange, mockConsumerDeadLetterExchangeWithArguments],
      publisherMetas: [mockPubliserMeta]
    })

    const config = result?.vhosts?.["/"]

    expect(config?.queues).toEqual(expect.objectContaining({
      "test.test-queue": {
        "options": {
          "deadLetterExchange": "test.test-dead-letter-exchange",
          "messageTtl": 10000
        }
      },
      "test.test-queue-dead-letter": {},
      "test.test-queue-arguments": {
        "options": {
          "arguments": {
            "x-message-ttl": 10000,
            "x-dead-letter-exchange": "test.test-dead-letter-exchange"
          }
        }
      }
    }))


    expect(config?.bindings).toEqual(expect.objectContaining({
      "test-exchange-test-queue": {
        "source": "test.test-exchange",
        "destination": "test.test-queue"
      },
      "test-dead-letter-exchange-test-queue-dead-letter": {
        "source": "test.test-dead-letter-exchange",
        "destination": "test.test-queue-dead-letter"
      },
      "test-exchange-test-queue-arguments": {
        "source": "test.test-exchange",
        "destination": "test.test-queue-arguments"
      }
    }))


  })
})


