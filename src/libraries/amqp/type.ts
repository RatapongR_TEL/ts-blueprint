import { Message } from 'amqplib';
import { ISubscriptionSession } from './rascalBroker';
import { IPublicationConfig, IQueueConfig, ISubscriptionConfig } from './rascalConfig';

export type PublicationMessage = object | IPublicationMessage;

export type IPublicationMessage = {
  message: any,
  config?: IPublicationConfig
}

export type SubscriptionMessage = any

export type MessageCallback = (content: SubscriptionMessage, message: Message) => Promise<void>;

export type  RascalStatusText = '' | 'connected' | 'error' | 'blocked';

export interface IRascalStatus {
  status: RascalStatusText;
  statusMessage?: any;
}

export interface IBroker {
  status: IRascalStatus;
  publish(pubConfig: IPublicationConfig, message: PublicationMessage): Promise<string>
  subscribe(queueConfig: IQueueConfig, cb: MessageCallback, context?: any, subConfig?: ISubscriptionConfig): Promise<ISubscriptionSession>;
  shutdown(): Promise<void>;
}

export const FORWARD = 'FORWARD'
export const REQUEUE = 'REQUEUE'
export const REPUBLISH = 'REPUBLISH'
export const REJECT = 'REJECT'

// set error to message before nack but cannot requeue back
export const REJECT_WITH_ERROR_INFO = 'REJECT_WITH_ERROR_INFO'

interface ForwardStrategy {
  type: typeof FORWARD
  attempts?: number
  exchange: string
}

interface RequeueStrategy {
  type: typeof REQUEUE
}

interface RepublishStrategy {
  type: typeof REPUBLISH
  attempts?: number
}

interface RejectStrategy {
  type: typeof REJECT
}

interface RejectWithErrorStrategy {
  type: typeof REJECT_WITH_ERROR_INFO
}

export type ErrorHandlingStrategy = ForwardStrategy | RepublishStrategy | RequeueStrategy | RejectWithErrorStrategy | RejectStrategy

export interface IConsumerMeta {
  type: object
  propertyKey: string
  subConfig?: ISubscriptionConfig
  queueConfig: IQueueConfig;
}

export interface IPublisherMeta {
  pubConfig: IPublicationConfig
}

export interface IConsumerFactory {
  get(consumerMeta: IConsumerMeta): any
}