import { withDefaultConfig } from 'rascal';
import { ILogger } from '../logger/logger.interface';
import { TraceUtil } from '../tracing/traceUtil';
import { AmqpConnection } from './connection';
import { DiConsumerFactory } from './diConsumerFactory';
import { MetaDataRegistry } from './metaDataRegistry';
import { RascalBroker } from './rascalBroker';
import { buildConfig, ISubscriptionConfig } from './rascalConfig';

export interface IAmqpConnectionOptions {
  connectionUrl: string
  username?: string
  password?: string
  prefix?: string
  defaultSubscription?: ISubscriptionConfig
  defaultReplyTimeout?: number
}

export async function createAmqpConnection(options: IAmqpConnectionOptions, logger: ILogger, tracer?: TraceUtil) {
  if (options.prefix) options.prefix += '.';

  const rascalConfig = withDefaultConfig(buildConfig({
    ...options,
    consumerMetas: MetaDataRegistry.consumerMetas,
    publisherMetas: MetaDataRegistry.publisherMetas,
  }));

  const broker = new RascalBroker(rascalConfig, logger, tracer);
  await broker.connect();

  const conn = new AmqpConnection(broker);

  // subscribe all registered  consumers
  await conn.subscribeAllConsumers(new DiConsumerFactory());

  return conn;
}
