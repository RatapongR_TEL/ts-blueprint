import { mock } from "jest-mock-extended";
import { mockClear } from 'jest-mock-extended/lib';
import { AmqpConnection } from './connection';
import { IBroker } from './type';


describe("AMQP connection", () => {

  const brokerMock = mock<IBroker>()

  beforeEach(() => {
    mockClear(brokerMock)
  })

  it("should creates AmqpConnection successfully", () => {
    const amqpConn = new AmqpConnection(brokerMock)
    expect(amqpConn).toBeDefined()
    expect(AmqpConnection.connection instanceof AmqpConnection).toBe(true)
  })
})