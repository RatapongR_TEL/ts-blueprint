import Container from 'typedi';
import { IConsumerFactory } from './type';

export class DiConsumerFactory implements IConsumerFactory {
  get(consumerMeta: import("./type").IConsumerMeta) {
    return Container.get(consumerMeta.type);
  }

}