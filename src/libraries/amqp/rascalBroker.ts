import { Message } from 'amqplib';
import { EventEmitter } from 'events';
import { Span } from 'opentracing';
import { BrokerAsPromised as Broker, BrokerConfig } from 'rascal';
import * as uuid from 'uuid';
import { ILogger } from '../logger/logger.interface';
import { TraceUtil } from '../tracing/traceUtil';
import { handleOnMessageError } from './errorHandler';
import { InvalidMessage, PublishReplyError } from './errorType';
import { IPublicationConfig, IQueueConfig, ISubscriptionConfig } from './rascalConfig';
import { IBroker, IRascalStatus, MessageCallback, PublicationMessage, RascalStatusText } from './type';
import { invokePromiseOrFunction } from './util';

export interface IPublishEventemitter extends EventEmitter {
  once(event: 'success', listener: (messageId: string) => void): this;
  once(event: 'error', listener: (err: Error, messageId: string) => void): this;
  once(event: 'return', listener: (message: Message) => void): this;
}

export interface ISubscriptionSession extends EventEmitter {
  name: string;
  isCancelled(): boolean;
  cancel(): Promise<void>;
  on(event: 'message', listener: (message: Message, content: any, ackOrNackFn: any) => void): this;
  on(event: 'error' | 'cancelled', listener: (err: Error) => void): this;
  on(event: 'invalid_content' | 'redeliveries_exceeded' | 'redeliveries_error' | 'redeliveries_error', listener: (err: Error, message: Message, ackOrNackFn: any) => void): this;
}

export interface IRascalBroker extends EventEmitter {
  readonly config: BrokerConfig;
  connect(name: string): Promise<any>;
  nuke(): Promise<void>;
  purge(): Promise<void>;
  shutdown(): Promise<void>;
  bounce(): Promise<void>;
  unsubscribeAll(): Promise<void>;
  publish(name: string, message: any, overrides?: IPublicationConfig | string): Promise<IPublishEventemitter>;
  forward(name: string, message: any, overrides?: IPublicationConfig | string): Promise<IPublishEventemitter>;
  subscribe(name: string, overrides?: ISubscriptionConfig): Promise<ISubscriptionSession>;
}

export class RascalBroker implements IBroker {
  private broker!: IRascalBroker;
  status: IRascalStatus = { status: '' };

  constructor(
    private brokerConfig: BrokerConfig,
    private logger: ILogger,
    private tracer?: TraceUtil) { }

  private setStatus(status: RascalStatusText, statusMessage?: any) {
    this.status = { status, statusMessage };
  }

  async connect() {
    this.broker = await Broker.create(this.brokerConfig);
    this.setStatus('connected');

    this.broker.on('error', (err: any) => {
      this.setStatus('error', err);
      this.logger.error(err, { event: 'rascal_error', error_type: 'broker_config' });
    });

    this.broker.on('vhost_initialised', ({ vhost, connectionUrl }) => {
      this.setStatus('connected', 'vhost_initialised');
      this.logger.warn(`Vhost: ${vhost} was initialised using connection: ${connectionUrl}`);
    });

    this.broker.on('blocked', (reason, { vhost, connectionUrl }) => {
      this.setStatus('blocked', reason);
      this.logger.warn(`Vhost: ${vhost} was blocked using connection: ${connectionUrl}. Reason: ${reason}`);
    });

    this.broker.on('unblocked', ({ vhost, connectionUrl }) => {
      this.setStatus('connected', 'unblocked');
      this.logger.warn(`Vhost: ${vhost} was unblocked using connection: ${connectionUrl}.`);
    });

    return this.broker;
  }

  private injectTracingContext(config: IPublicationConfig) {
    if (this.tracer) {
      // inject tracing context
      config.options = config.options || {};
      config.options.headers = config.options.headers || {};
      this.tracer.injectTracingContext(config.options.headers);
    }
  }

  private startSpan(queueConfig: IQueueConfig, message: Message) {
    if (!this.tracer) return;

    return this.tracer.startSpanFromHeader(
      `amqp: ${queueConfig.queueName}`,
      message.properties.headers,
      {
        type: 'amqp', tags: {
          queue: queueConfig.queueName,
          exchange: queueConfig.exchange || '',
          routingKey: queueConfig.routingKey || '',
        }
      })
  }

  private finishSpan(span?: Span, err?: any) {
    if (!span || !this.tracer) return;

    this.tracer.finishSpan(span, err);
  }

  async publish(config: IPublicationConfig, message: PublicationMessage): Promise<any> {
    let content = message;

    if ('message' in message) {
      content = message.message;
      const options = { ...config.options, ...message.config?.options };
      Object.assign(config, { ...message.config, options });
    }

    this.injectTracingContext(config);

    const name = config.exchange || config.queue || '/';

    if (config.options?.replyTo) {
      config.options.correlationId = config.options.correlationId || uuid.v4();
      return this.publishAndWaitForReply(name, content, config);
    }

    return this.publishQueue(name, content, config);
  }

  private async publishQueue(name: string, content: any, config: IPublicationConfig) {
    const publication = await this.broker.publish(name, content, config);

    return new Promise((resolve, reject) => {
      publication.once('error', (err: Error, _messageId: string) => {
        this.setStatus('error', err);
        this.logger.error(err, { event: 'rascal_error', error_type: 'publish_error', exchange: config.exchange, queue: config.queue });
        reject(err);
      });

      publication.once('success', (messageId) => {
        resolve(messageId);
      });
    });
  }

  private subscribedReplies: any = {};
  private pendingReplies: { [key: string]: any } = {};

  private async subscribeReply(config: IPublicationConfig) {
    const replyQueue = config.options?.replyTo!;

    if(this.subscribedReplies[replyQueue]) return;

    const sub = await this.subscribe({ queueName: replyQueue }, async (content: any, message: Message) => {
      const correlationId = message.properties.correlationId;
      const key = `${replyQueue}-${correlationId}`
      const replyRequest = this.pendingReplies[key];

      if (!replyRequest) {
        this.logger.debug(`message key ${key} does not match on pending replies`);
        return;
      }

      const err = message.properties?.headers?.error;
      if (err) {
        this.logger.debug(err, 'reply request error');
        replyRequest.reject(err);
      } else {
        replyRequest.resolve(content);
      }

      clearTimeout(replyRequest.timeout);
      delete this.pendingReplies[key];
    });

    this.subscribedReplies[replyQueue] = sub;

    return sub;
  }

  private registerPendingReply(config: IPublicationConfig) {
    return new Promise((resolve, reject) => {
      const replyQueue = config.options?.replyTo!;
      const correlationId = config.options?.correlationId!;
      const key = `${replyQueue}-${correlationId}`;

      const timeout = setTimeout(() => {
        this.logger.error(`reply queue receive timeout on ${key}`);

        delete this.pendingReplies[key];
        reject(`reply queue receive timeout on ${key}`);
      }, config.replyTimeout);

      this.pendingReplies[key] = { resolve, reject, timeout };
    });
  }

  private async publishAndWaitForReply(name: string, content: any, config: IPublicationConfig) {
    await this.subscribeReply(config);

    const pendingReply = this.registerPendingReply(config);

    await this.publishQueue(name, content, config);

    return pendingReply;
  }

  private hasReplyTo(message: Message): boolean {
    const { replyTo, correlationId } = message.properties;
    return replyTo && correlationId;
  }

  private async publishReplyIfNeeded(message: Message, result: any, err?: any) {
    const { replyTo, correlationId } = message.properties;

    if(!this.hasReplyTo(message)) return false;

    try {
      const config: IPublicationConfig = { routingKey: replyTo, options: { correlationId } };

      if (err) {
        Object.assign(config.options, { headers: { error: err.toString() } } );
      }

      await this.publish({ queue: '/' }, { message: result, config });

      return true;
    } catch (err) {
      this.logger.error(err, 'Publishing reply error');
      throw new PublishReplyError(err);
    }
  }

  private async handleMessage(message: Message, content: any, cb: MessageCallback, context?: any) {
    try {
      const result = await invokePromiseOrFunction(cb, context, [content, message]);
      await this.publishReplyIfNeeded(message, result);
    } catch (err) {
      if (err instanceof PublishReplyError) {
        throw err;
      }

      const hasReplyTo = await this.publishReplyIfNeeded(message, {}, err);
      if (hasReplyTo) return;

      throw err;
    }
  }

  async subscribe(queueConfig: IQueueConfig, cb: MessageCallback, context?: any, config?: ISubscriptionConfig) {
    const subscription = await this.broker.subscribe(queueConfig.queueName, config);
    subscription.on('message', async (message: Message, content: any, ackOrNack: any) => {
      const span = this.startSpan(queueConfig, message);

      try {
        await this.handleMessage(message, content, cb, context);

        ackOrNack();

        this.finishSpan(span);
      } catch (err) {
        handleOnMessageError(err, ackOrNack, message, config?.errorStrategy);

        this.finishSpan(span, err);
      }
    })

    subscription.on('invalid_content', (err: any, message: Message, ackOrNack: any) => {
      this.logger.error(err, { event: 'rascal_error', error_type: 'invalid_content', queue: queueConfig.queueName });
      handleOnMessageError(new InvalidMessage(err), ackOrNack, message);
    })

    subscription.on('error', (err: any) => {
      this.setStatus('error', err);
      this.logger.error(err, { event: 'rascal_error', error_type: 'subscribe_config', queue: queueConfig.queueName });
    });

    return subscription;
  }

  shutdown() {
    return this.broker.shutdown();
  }

}