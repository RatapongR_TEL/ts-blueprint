import { AmqpConnection } from '../connection';
import { amqpPublish } from './amqpPublisher';

jest.mock('../connection');

describe('Amqp publisher decorator', () => {
  // @ts-ignore
  class TestClass {
    @amqpPublish('testq')
    async publish(param: string) {
      return `hello ${param}`;
    }
  }

  it('should call publish function instead of original', async () => {
    const mockPublish = AmqpConnection.PUBLISH as jest.Mock;
    mockPublish.mockResolvedValue('mocked result');

    const test = new TestClass();
    const result = await test.publish('m1');

    expect(result).toBe('mocked result');

    const param0 = mockPublish.mock.calls[0][0]
    const param1 = mockPublish.mock.calls[0][1]
    const param2 = mockPublish.mock.calls[0][2]
    const param3 = mockPublish.mock.calls[0][3]

    expect(param0).toEqual({ "queue": "testq" });
    expect(typeof param1).toBe('function');
    expect(param1.name).toBe('publish');
    expect(param2).toEqual(['m1']);
    expect(param3).toBe(test);
  })

  it('should throw error to original function', async () => {
    const mockPublish = AmqpConnection.PUBLISH as jest.Mock;
    mockPublish.mockRejectedValue(new Error('test error'));

    const test = new TestClass();

    const check = () => {
      return test.publish('m1');
    }

    expect(check()).rejects.toThrow(new Error('test error'));
  })
})