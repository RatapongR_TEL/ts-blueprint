import { CacheManager } from '../cacheManager';
import { cacheKey } from './cacheKey';
import { evictCache } from './evictCache';
import { ICacheEvictMeta } from './meta';

jest.mock('../cacheManager');

describe('@evictCache decorator', () => {
  const mockWrapCacheEvict = CacheManager.WRAP_CACHE_EVICT as jest.Mock;

  beforeEach(() => {
    mockWrapCacheEvict.mockReset();
  })

  it('should wrap cache with method name as key', () => {
    // @ts-ignore
    class TestClass {
      @evictCache('key1')
      async test1(_id: string) { }
    }

    assertWrapCacheCall('test1', {
      cacheEvictOption: { key: 'key1' },
      cacheKeyMetas: []
    })
  })

  it('should wrap cache @cacheKey', () => {
    // @ts-ignore
    class TestClass {
      @evictCache('key1')
      async test1(@cacheKey() _id: string) { }
    }

    assertWrapCacheCall('test1', {
      cacheEvictOption: { key: 'key1' },
      cacheKeyMetas: [{ parameterIndex: 0 }]
    })
  })

  function assertWrapCacheCall(expectedFunctionName: string, expectMeta: ICacheEvictMeta) {
    expect(mockWrapCacheEvict).toHaveBeenCalledTimes(1);

    const call1 = mockWrapCacheEvict.mock.calls[0];
    expect(call1).toHaveLength(2);

    const arg1 = call1[0];
    const arg2 = call1[1];

    expect(arg1).toBeDefined();
    expect(typeof arg1).toBe('function');
    expect(arg1.name).toBe(expectedFunctionName);
    expect(arg2).toBeDefined();
    expect(arg2).toEqual(expectMeta);
  }

})
