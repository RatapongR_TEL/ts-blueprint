import { CacheManager } from '../cacheManager';
import { ICacheOptions } from '../interface';
import { CacheMeta } from './meta';

type CacheMethodDecorator<E extends any[]> =
  (target: Object,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<(...args: E) => Promise<any>>) => void

export function cache<E extends any[]>(options?: ICacheOptions<E>): CacheMethodDecorator<E> {
  return (target, propertyKey, descriptor) => {
    const cacheOptions = options || { key: propertyKey };

    const cacheMeta = CacheMeta.GET_CACHE_META(target, propertyKey, cacheOptions)

    descriptor.value = CacheManager.WRAP_CACHE(descriptor.value!, cacheMeta);
  }
}