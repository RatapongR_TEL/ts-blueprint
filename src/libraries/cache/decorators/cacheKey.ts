import { CacheMeta } from './meta';

type CacheKeyDecorator = (target: Object, propertyKey: string, parameterIndex: number) => void

export function cacheKey(key?: string): CacheKeyDecorator {
  return (target, propertyKey, parameterIndex) => {
    CacheMeta.ADD_CACHEKEY_META(target, propertyKey, parameterIndex, key);
  }
}