import { CacheManager } from '../cacheManager';
import { CacheMeta } from './meta';

type CacheEvictMethodDecorator<E extends any[]> =
  (target: Object,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<(...args: E) => Promise<any>>) => void

export function evictCache<E extends any[]>(key: string | ((...args: E) => string)): CacheEvictMethodDecorator<E> {
  return (target, propertyKey, descriptor) => {
    const cacheEvictOptions = { key };

    const cacheEvictMeta = CacheMeta.GET_CACHE_EVICT_META(target, propertyKey, cacheEvictOptions)

    descriptor.value = CacheManager.WRAP_CACHE_EVICT(descriptor.value!, cacheEvictMeta);
  }
}