export type CacheKey<E extends any[]> = string | ((...args: E) => string);

export interface ICacheConfig {
  /**
     * time to live in seconds
     */
  ttl?: number
  /**
   * maximum number of objects in cache
   */
  max?: number
}

export interface ICacheOptions<E extends any[]> extends ICacheConfig {
  key: CacheKey<E>
}

export interface ICacheEvictOptions<E extends any[]> {
  key: CacheKey<E>
}

export interface ICacheProvider {
  wrap(key: string, cb: () => any, option?: ICacheConfig): Promise<any>
  del(key: string, cb: (err: any) => void): void
}